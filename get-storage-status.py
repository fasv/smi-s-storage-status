#! /usr/bin/python3

import yaml, json, argparse, pywbem, os, sys
scriptpath = os.path.dirname(sys.argv[0])
conffile = scriptpath + '/smi-s.cfg.yml'
devicedescription = ''
result=[]

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--address',   dest='address',   required=True)
parser.add_argument('-u', '--username',  dest='username',  required=True)
parser.add_argument('-p', '--password',  dest='password',  required=True)
parser.add_argument('-m', '--model',     dest='model',     required=True)
parser.add_argument('-s', '--subsystem', dest='subsystem', required=True)
parser.add_argument('-c', '--config',    dest='config')
parser.add_argument('-i', '--ignore',    dest='ignore', default=False, action='store_true')

args = parser.parse_args()

if args.config != None:
    conffile = args.config

# Read config file
with open(conffile, 'r') as conf:
    config = yaml.safe_load(conf)

for item in config:
    if item['model'] == args.model:
        devicedescription = item

if devicedescription == '':
    print('There are no description in config file for device you have entered. Please check correctnes of device name or fill description for them')
    exit()

if args.ignore == True:
    conn = pywbem.WBEMConnection(args.address, (args.username, args.password), default_namespace=devicedescription.get('namespace'), no_verification=True)
    # connection_string = f"'{args.address}', ({args.username}, {args.password}), default_namespace='{devicedescription.get('namespace')}', no_verification=True"
else:
    conn = pywbem.WBEMConnection(args.address, (args.username, args.password), default_namespace=devicedescription.get('namespace'))

instances = conn.EnumerateInstanceNames(devicedescription['subsystems'][args.subsystem]['class'])

if len(instances) == 0:
    print('There are no instances of this class. Exiting')
    exit()

for instance in instances:
    instanceitems = {}
    for item in devicedescription['subsystems'][args.subsystem]['items']:
        if isinstance(conn.GetInstance(instance).get(item), str):
            instanceitems[item] = conn.GetInstance(instance).get(item)

        elif isinstance(conn.GetInstance(instance).get(item), list) and len(conn.GetInstance(instance).get(item)) == 1:
            instanceitems[item] = conn.GetInstance(instance).get(item)[0].real

        elif isinstance(conn.GetInstance(instance).get(item), pywbem._cim_types.Uint16):
            instanceitems[item] = conn.GetInstance(instance).get(item).real
        else:
            print('The remote system return unrecognized reply type(not str and not list with one item. Exiting)')
            exit()

    result.append(instanceitems)
print(json.dumps(result))